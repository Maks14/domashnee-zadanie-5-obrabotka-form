<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
// получаем все имена файлов и каталогов в папке
// обрезаем ненужные элементы у массива (ключи не меняется)
// меняем нумерацию у массива
$tests = array_values(array_diff(scandir('upload'), ['..', '.']));                    
?>

<?php 
if(!empty($_GET)) {
    $testNumber = (int)$_GET['test'];
    if($testNumber > 0 && $testNumber <= count($tests)) { 
    $test = $tests[$testNumber -1];  // выбираем тест из массива tests (-1 потому что первый тест под индексом 0)
    $test = file_get_contents("upload/$test"); // считываем содержимое теста
    $test = json_decode($test, true); // декодируем
    // var_dump($test);
?>

<!-- Выводим тест на экран, и отправляем ответы пользователя методом post -->
        <form action="" method="post">
        <?php $i = 0; ?>    
        <?php foreach($test[0] as $key) { 
                foreach($key as $questions => $value) {?>
        <fieldset>
            <legend><?php echo $questions; $i++; ?></legend>
            <?php foreach($value as $answer) { ?>
            <label><input type="radio" name="answer<?php echo $i ?>" value = "<?php echo $answer ?>"><?php echo $answer; ?></label>
            <?php } ?>
        </fieldset>
            <?php } ?> 
        <?php } ?> 
            <p>Введите ваше имя: <input type="text" name="user_name"></p>
            <?php if(!empty($_POST['user_name'])) {
                    } else {
                    echo 'Введите пожалуйста ваше имя!';
            }   
            ?>
        <input type="submit" value="Отправить">
        </form> 

<?php } else if ($testNumber <= 0) {
    echo 'Введите корректные данные';
        } else {
        http_response_code(404);
        echo 'not found';
        } 
}

?>

<?php
// ----------------------ПРОВЕРКА ОТВЕТОВ -------------------
$answer_user = [];
$answer_true = $test[1]['correct answers'];
$result = 0;

// Ответы пользователя получаем и записываем в массив
for($i = 1; $i <count($_POST); $i++) {
    if(is_numeric($_POST['answer' . $i])) {
        $_POST['answer' . $i] *= 1;
    }
    array_push($answer_user, $_POST['answer' . $i]);
}

for($index = 0; $index < count($answer_true); $index++) {
    if(!empty($answer_user[$index]))  {
        if($answer_user[$index] === $answer_true[$index]) {
            $result += 1;
        } 
        echo 'Ответ пользователя: ' . $answer_user[$index];
        echo ' Правильный ответ: ' . $answer_true[$index];
        echo "<br>";
    }
}

if(!empty($answer_user)) {
    echo "<br>";
    echo "Вы ответили на $result правильно";
}
// ------------------------------------------//
?>
</body>
</html>